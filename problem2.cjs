const inventory = require("./inventory.cjs");

const lastIndex = inventory.length - 1;

function problem2(inventory) {

    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    return inventory[lastIndex];
    // return `Last car is a ${inventory[lastIndex].car_make} ${inventory[lastIndex].car_model}`;
}

// console.log(problem2(inventory));

module.exports = problem2;