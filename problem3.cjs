const inventory = require("./inventory.cjs");

function problem3(inventory) {
    const result = [];

    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    for (let index = 0; index < inventory.length; index++) {
        result.push(inventory[index].car_model);
    }

    for (let index = 0; index < result.length-1; index++) {
        for (let secondIndex = index+1; secondIndex < result.length; secondIndex++) {
            let temp = '';
            if (result[index] > result[secondIndex]) {
                temp = result[index];
                result[index] = result[secondIndex];
                result[secondIndex] = temp;
            }
        }
    }
    return result;
}

// console.log(problem3(inventory));

module.exports = problem3;