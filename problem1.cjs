const inventory = require("./inventory.cjs");

// const checkId = 33;

function problem1(inventory, checkId) {

    if (arguments.length !== 2 || inventory.length === 0 || !Array.isArray(inventory) || (checkId < 1 && checkId > 50) || typeof(checkId) != 'number') {
        return [];
    }

    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id === checkId) {
            return inventory[index];
            // `Car 33 is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`
        }
    }
    // return [];
}

// console.log(problem1(inventory, checkId));

module.exports = problem1;