
const data = require('../inventory.cjs')
const problem2 = require('../problem2.cjs')

test('Testing problem2', () => {
    expect(problem2(data)).toStrictEqual({"car_make": "Lincoln", "car_model": "Town Car", "car_year": 1999, "id": 50})
});

// Last car is a Lincoln Town Car