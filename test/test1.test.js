const data = require('../inventory.cjs');
const problem1 = require('../problem1.cjs');

test('Testing problem1', () => {
    expect(problem1(data,33)).toStrictEqual({"car_make": "Jeep", "car_model": "Wrangler", "car_year": 2011, "id": 33})
});

//output = Car 33 is a 2011 Jeep Wrangler
