const inventory = require("./inventory.cjs");
const problem4 = require("./problem4.cjs");

const prev = problem4(inventory);
// console.log(prev);
function problem5(prev) {
    const result = [];

    if (arguments < 1 || prev.length === 0 || !Array.isArray(prev)) {
        return [];
    }

    for (let index = 0; index < prev.length; index++) {
        if (prev[index] < 2000) {
            result.push(prev[index]);
        }
    }
    return result.length;
}

// console.log(problem5(prev));

module.exports = problem5;