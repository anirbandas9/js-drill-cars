const inventory = require("./inventory.cjs");

function problem4(inventory) {
    const years = [];

    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    for (let index = 0; index < inventory.length; index++) {
        years.push(inventory[index].car_year);
    }
    // return Array.from(years);
    return years;
}

// console.log(problem4(inventory));

module.exports = problem4;
// module.exports.inventory = inventory;